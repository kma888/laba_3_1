#include "gtest/gtest.h"
#include "listsequence.h"
#include "arraysequence.h"
#include "iSorter.h"

class SortAlgoTest : public ::testing::Test {
public:
    ~SortAlgoTest() = default;
protected:
    virtual void CreateNewRandom(int N){

        AS = new ArraySequence<int>();
        LS = new ListSequence<int>();
        my_vector.clear();

        int *arr = new int[N];
        for (int i=0; i<N; i++){
            arr[i] = rand()%N + 1;
            AS->Append(arr[i]);
            LS->Append(arr[i]);
            my_vector.push_back(arr[i]);
        }
        sort(my_vector.begin(), my_vector.end());
    }

    ArraySequence<int> *AS;
    ListSequence<int> *LS;
    std::vector<int> my_vector;
};

TEST_F(SortAlgoTest, SimpleSelectionSort) {
    iSorter<int> *sorter = new SimpleSelectionSort<int>;
    for (int i=0; i<1000; i+=100)
        CreateNewRandom(i);
    EXPECT_EQ(sorter->Sort(AS, iCompare<int>::Sort)->ToVector(), my_vector);
    EXPECT_EQ(sorter->Sort(LS, iCompare<int>::Sort)->ToVector(), my_vector);
}

TEST_F(SortAlgoTest, BinarySort) {
    iSorter<int> *sorter = new BinarySort<int>;
    for (int i=0; i<1000; i+=100)
        CreateNewRandom(i);
    EXPECT_EQ(sorter->Sort(AS, iCompare<int>::Sort)->ToVector(), my_vector);
    EXPECT_EQ(sorter->Sort(LS, iCompare<int>::Sort)->ToVector(), my_vector);
}

TEST_F(SortAlgoTest, SquareSort) {
    iSorter<int> *sorter = new SquareSort<int>;
    for (int i=0; i<1000; i+=100)
        CreateNewRandom(i);
    EXPECT_EQ(sorter->Sort(AS, iCompare<int>::Sort)->ToVector(), my_vector);
    EXPECT_EQ(sorter->Sort(LS, iCompare<int>::Sort)->ToVector(), my_vector);
}

TEST_F(SortAlgoTest, QuickSort) {
    iSorter<int> *sorter = new QuickSort<int>;
    for (int i = 0; i < 1000; i += 100)
        CreateNewRandom(i);
    EXPECT_EQ(sorter->Sort(AS, iCompare<int>::Sort)->ToVector(), my_vector);
    EXPECT_EQ(sorter->Sort(LS, iCompare<int>::Sort)->ToVector(), my_vector);
}

TEST_F(SortAlgoTest, HeapSort) {
    iSorter<int> *sorter = new HeapSort<int>;
    for (int i=0; i<1000; i+=100)
        CreateNewRandom(i);
    EXPECT_EQ(sorter->Sort(AS, iCompare<int>::Sort)->ToVector(), my_vector);
    EXPECT_EQ(sorter->Sort(LS, iCompare<int>::Sort)->ToVector(), my_vector);
}