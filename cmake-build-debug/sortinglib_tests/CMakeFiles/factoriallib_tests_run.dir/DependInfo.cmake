# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/mihail/laba_3_1/sortinglib_tests/falgo_test.cpp" "/Users/mihail/laba_3_1/cmake-build-debug/sortinglib_tests/CMakeFiles/factoriallib_tests_run.dir/falgo_test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../sortinglib"
  "../sortinglib_tests/lib/googletest/include"
  "../sortinglib_tests/lib/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/mihail/laba_3_1/cmake-build-debug/sortinglib_tests/lib/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/Users/mihail/laba_3_1/cmake-build-debug/sortinglib_tests/lib/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
