#pragma once
#include "sequence.h"
#include "iCompare.h"
using namespace std;

template<class T>
class iSorter{
public:
    virtual Sequence<T> *Sort(Sequence<T> *seq, int (*cmp)(T,T)) = 0;
};


template<typename T>
//Сортировка с помощью простого выбора
class SimpleSelectionSort : public iSorter<T> {

private:
    Sequence<T> *Sort(Sequence<T> *seq, int (*cmp)(T,T)) override {
        for (int startIndex = 0; startIndex < seq->GetSize() - 1; ++startIndex) {
            int smallestIndex = startIndex;
            for (int currentIndex = startIndex + 1; currentIndex < seq->GetSize(); ++currentIndex) {
                if (cmp((*seq)[currentIndex], (*seq)[smallestIndex]) == -1)
                    smallestIndex = currentIndex;
            }
            seq->Swap(startIndex, smallestIndex);
        }
        return seq;
    }
};


template<typename T>
//Метод двоичных (бинарных) вставок
class BinarySort : public iSorter<T> {

private:
    Sequence<T> *Sort(Sequence<T> *seq, int (*cmp)(T, T)) override{
        T x;
        int left;
        int right;
        int sred;
        for (int i = 1; i < seq->GetSize(); i++) {
            if (cmp((*seq)[i - 1], (*seq)[i]) == 1) {
                x = (*seq)[i];
                left = 0;
                right = i - 1;
                do {
                    sred = (left + right) / 2;
                    if (cmp((*seq)[sred], x) == -1) left = sred + 1;
                    else right = sred - 1;
                } while (left <= right);
                for (int j = i - 1; j >= left; j--)
                    (*seq)[j + 1] = (*seq)[j];
                (*seq)[left] = x;
            }
        }
        return seq;
    }
};

//Квадратичная сортировка (усов. сортировка выбором)
template<typename T>
class SquareSort : public iSorter<T> {

private:
    Sequence<T> *Sort(Sequence<T> *seq, int (*cmp)(T, T)) override {

        //Массив А из N элементов, разделяется на "корень из N" групп, по "корень из N" элементов в каждой группе.
        // Находится наименьший элемент в каждой группе и помещается в некоторый вспомогательный массив В.
        // Далее во вспомогательном массиве находится минимальный элемент.
        // Данный элемент заносится на следующую позицию выходного массива С, и в массиве В заменяется на следующий по
        // величине элемент из этой группы массива А, из которой он поступил. –
        // Снова находится наименьший элемент во вспомогательном массиве. Этот элемент - второй по величине в исходном массиве.
        // Процесс повторяется до тех пор, пока ис ходных массив не будет отсортирован. В данном методе используются
        // 3 массива, А - исходный, В - вспомогательный, С - выходной.

        int nGroups = sqrt(seq->GetSize());
        if (nGroups * nGroups < seq->GetSize())
            nGroups++;


        Sequence<T> **groupList = new Sequence<T> *[nGroups];
        Sequence<T> *resultList1 = seq->New(); //вспомогательный массив
        Sequence<T> *resultList2 = seq->New(); //выходной массив

        int START = 0, STEP = round(sqrt(seq->GetSize())), LEN = seq->GetSize();

        for (int i = 0; i < nGroups; ++i) {
            if (i == nGroups - nGroups * STEP + LEN) {
                --STEP;
            }
            groupList[i] = seq->GetSubsequence(START, START + STEP); //Включительно или нет?
            START += STEP;
        }

        for (int j = 0; j < nGroups; ++j) {
            int min_index = groupList[j]->FindMin(cmp);
            resultList1->Append((*groupList[j])[min_index]);
            groupList[j]->Delete(min_index);
        }

        while (resultList1->GetSize() > 0) {
            int min_index = resultList1->FindMin(cmp);
            resultList2->Append((*resultList1)[min_index]);
            if (groupList[min_index]->GetSize() == 0) {
                for (int k = min_index; k < nGroups; k++) {
                    groupList[k] = groupList[k + 1];
                }
                nGroups--;
                resultList1->Delete(min_index);
            } else {
                int min_index2 = groupList[min_index]->FindMin(cmp);
                T &item = (*groupList[min_index])[min_index2];
                resultList1->Set(item, min_index);
                groupList[min_index]->Delete(min_index2);
            }
        }
        return resultList2;
    }
};

//Быстрая сортировка QuickSort
template<class T>
class QuickSort : public iSorter<T> {

private:
    Sequence<T> *QuickSort_help(Sequence<T> *seq, int (*cmp)(T, T), int b, int e) {
        if (seq->GetSize() == 1 || seq->GetSize() == 0) {
            return seq;
        }
        int l = b, r = e;
        T piv = (*seq)[(l + r) / 2];// Опорным элементом для примера возьмём средний
        while (l <= r) {
            while (cmp((*seq)[l], piv) == -1) l++;
            while (cmp((*seq)[r], piv) == 1) r--;
            if (l <= r)
                seq->Swap(l++, r--);
        }
        if (b <= r) {
            QuickSort_help(seq, cmp, b, r);
        }
        if (e >= l) {
            QuickSort_help(seq, cmp, l, e);
        }
    }

    Sequence<T> *Sort(Sequence<T> *seq, int (*cmp)(T, T)) override {
        return QuickSort_help(seq, cmp, 0, seq->GetSize() - 1);
    }
};

//Пирамидальная сортировка
//HeapSort
//a[i] <= a[2i+1]      a[i] <= a[2i+2]
template<class T>
class HeapSort : public iSorter<T> {

private:
    void heapify(Sequence<T> *arr, int n, int i, int (*cmp)(T, T)) {
        int largest = i;
// Инициализируем наибольший элемент как корень
        int l = 2 * i + 1; // левый = 2*i + 1
        int r = 2 * i + 2; // правый = 2*i + 2

        // Если левый дочерний элемент больше корня
        if (l < n && cmp((*arr)[l], (*arr)[largest]) == 1)
            largest = l;

        // Если правый дочерний элемент больше, чем самый большой элемент на данный момент
        if (r < n && cmp((*arr)[r], (*arr)[largest]) == 1)
            largest = r;

        // Если самый большой элемент не корень
        if (largest != i) {
            arr->Swap(i, largest);

// Рекурсивно преобразуем в двоичную кучу затронутое поддерево
            heapify(arr, n, largest, cmp);
        }
    }

// Основная функция, выполняющая пирамидальную сортировку
    Sequence<T> *Sort(Sequence<T> *arr, int (*cmp)(T, T)) override {
        int n = arr->GetSize();
        // Построение кучи (перегруппируем массив)
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(arr, n, i, cmp);

        // Один за другим извлекаем элементы из кучи
        for (int i = n - 1; i >= 0; i--) {
            // Перемещаем текущий корень в конец
            arr->Swap(0, i);

            // вызываем процедуру heapify на уменьшенной куче
            heapify(arr, i, 0, cmp);
        }

        return arr;
    }
};