#pragma once
#include "string"

template<class T>
class iCompare{
public:
    static int Sort(T x, T y){};
    static int SortReverse(T x, T y) { return Sort(x, y) * (-1); }
};

template<> int iCompare<int>::Sort(int x, int y) {
    if (x > y) {return 1; }
    if (x < y) {return -1;}
    else       {return 0; }
}

template<> int iCompare<double>::Sort(double x, double y) {
    if (x > y) {return 1; }
    if (x < y) {return -1;}
    else       {return 0; }
}

template<> int iCompare<string>::Sort(string x, string y) {
    if (x.length() > y.length()) {return 1; }
    if (x.length() < y.length()) {return -1;}
    else                         {return 0; }
}