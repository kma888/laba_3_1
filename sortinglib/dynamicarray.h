#include <iostream>
#pragma once
using namespace std;

template<class T>
class DynamicArray {
private:
  size_t size;
  T* items;

public:
  DynamicArray() : size(0), items(nullptr) {}

  DynamicArray(T* list, size_t size_) : size(size_), items(list) {}

  //////////////////////////////////////////////////////////////////////////

  void Set(T item, int index) { items[index] = item; }

  void Resize(size_t size_) {
    if (size_ > size) {
      auto new_items = new T[size_];
      for (int i = 0; i < size; ++i) {
        new_items[i] = items[i];
      }
      items = new_items;
    }
    size = size_;
  }

  void Delete (int index) {
      T* new_arr = new T[size-1];
      for (int i=0; i<size-1; ++i) {
          if (i<index) {
              new_arr[i] = items[i];
          }
          if (i>=index) {
              new_arr[i] = items[i+1];
          }
      }
      delete[] items;
      items = new_arr;
      size--;
  }

  T& operator[] (int index) const { return items[index]; }

  int FindMin(int (*cmp)(T,T)) {
      T* MinVal = new T;
      *MinVal = items[0];
      int counter_min = 0;
      for (int i=0; i<size; ++i) {
          if ( cmp(*MinVal, items[i]) == 1 ) {
              *MinVal = items[i];
              counter_min = i;
          }
      }

      return counter_min;
  }

  int FindMedian(int (*cmp)(T,T)) {

  }

  void Swap(int i, int j) {
      T tmp = items[i];
      items[i] = items[j];
      items[j] = tmp;
  }

  /*
  void where (bool(*func)(T&)){
    for (int i=0; i<size; i++){
      if(!func(items[i]->Get())) {
        for (int j = i; j < size-1; ++j)
          if (j<size-1) { items[j] = items[j + 1]; }
        size--;
        i--;
      }
    }
  }
  */

  /*
  Data<T>* reduce(Data<T>*(*func)(Data<T>&,Data<T>&)){
    auto new_data = Get(0)->copy_new();
    for (auto i=0; i<size; ++i){
      new_data = func(*new_data, *Get(i));
    }
    return new_data;
  }
   */

  int GetSize() const { return size; }

  ~DynamicArray() { delete[] items; }
};