#pragma once

#include "dynamicarray.h"
#include "sequence.h"

template<class T>
class ArraySequence : public Sequence<T> {
private:
  DynamicArray<T> items;
public:
  ArraySequence() : items() {}

  ArraySequence(const ArraySequence<T> &list) : items(list.items) {}

  explicit ArraySequence(int N) : items() {
      int *arr = new int[N];
      for (int i=0; i<N; ++i) {
          arr[i] = rand() % N + 1;
          Append(arr[i]);
      }
  }

  ////////////////////////////////////////////////////////////////////////////

  int FindMin(int (*cmp)(T,T)) override { return items.FindMin(cmp); }

  Sequence<T>* New() override {return new ArraySequence<T>(); }

  int GetSize() const override { return items.GetSize(); }

  void Resize(int size) override { items.Resize(size); }

  void Set(T data, int index) override { items.Set(data, index); }

  T& GetFirst() const override { return items[0]; }

  T& GetLast() const override { return items[GetSize()-1]; }

  T& operator[](int index) override { return items[index]; }

  void Append(T& item) override {
    int size = items.GetSize();
    items.Resize(size + 1);
    items.Set(item, size);
  }

  void Prepend(T& item) override {
    int size = items.GetSize();
    items.Resize(size + 1);
    for (int i = size-1; i > 0; i--)
      items.Set(items[i - 1], i);
    items.Set(item, 0);
  }

  void InsertAt(T& item, int index) override {
      assert(index >= 0 && index < GetSize() + 1);
    int size = items.GetSize();
    items.Resize(size + 1);
    for (int i = size; i > index; i--){
      items.Set(items[i - 1], i);
    }
    items.Set(item, index);
  }

  void Print() override {
    for (int i = 0; i < GetSize(); i++) {
      cout << items[i] << "\t";
    }
    cout << "\n";
  }

  Sequence<T> *Concat(Sequence<T> &list) override {
      ArraySequence<T> *conc = new ArraySequence<T>;
    conc->items.Resize(GetSize() + list.GetSize());
    for (int i = 0; i < items.GetSize(); i++)
      conc->items.Set(items[i], i);
    for (int i = 0; i < list.GetSize(); i++)
      conc->items.Set(list[i], i + items.GetSize());
    return conc;
  }

  Sequence<T> *GetSubsequence(int startIndex, int endIndex) override {
    auto *sub = new ArraySequence<T>;
    sub->items.Resize(endIndex - startIndex);
    for (int i = startIndex; i < endIndex; i++)
      sub->items.Set(items[i], i - startIndex);
    return sub;
  }

  Sequence<T> *Clone() override {
    auto new_items = new ArraySequence<T>();
    for (int i = 0; i < GetSize(); i++) {
        T* tmp = new T;
        *tmp = items[i];
        new_items->Append(*tmp);
    }
    return new_items;
  }

  void Swap(int i, int j) override { items.Swap(i, j); }

  /*
  Sequence<T> *CopyNew(int size) override {
    auto *new_items = new ArraySequence<T>();
    new_items->Resize(size);
    for (int i = 0; i < size; i++)
      new_items->Set(GetFirst()->copy_new(), i);
    return new_items;
  }
  */

  /*
  void Map(void(*func)(T&)) override{
    for (int i=0; i < GetSize(); i++)
      items[i]->map(func);
  };
  */

  /*
  virtual void Where(bool(*func)(T&)) override{
      items.where(func);
  };
  */

  /*
  Data<T>* Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) override{
    return items.reduce(func);
  }
  */

  virtual std::vector<T> ToVector() {
      std::vector<T> *vec = new std::vector<T>;
      for (int i=0; i<GetSize(); ++i){
          vec->push_back((*this)[i]);
      }
      return *vec;
  }

  void Delete(int index) override { items.Delete(index); }

  virtual ~ArraySequence<T>() = default;
};
