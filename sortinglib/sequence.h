#pragma once
#include "vector"

template<class T>
class Sequence {
public:
  virtual int GetSize() const = 0;

  virtual void Resize (int size) = 0;

  virtual void Set(T item, int index) = 0;

  virtual T& GetFirst() const = 0;

  virtual T& GetLast() const = 0;

  virtual T& operator[](int index) = 0;

  virtual void Append(T& item) = 0;

  virtual void Prepend(T& item) = 0;

  virtual void InsertAt(T& item, int index) = 0;

  virtual void Delete(int index) = 0;

  virtual void Print() = 0;

  virtual Sequence<T> *Concat(Sequence<T> &list) = 0;

  virtual Sequence<T> *GetSubsequence(int startIndex, int endIndex) = 0;

  virtual Sequence<T> *Clone() = 0;

  virtual Sequence<T> *New() = 0;

  //virtual Sequence<T>* CopyNew(int size) = 0;

  //virtual void Map(void(*func)(T&)) = 0;

  //virtual void Where(bool(*func)(T&)) = 0;

  //virtual Data<T>* Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) = 0;

  virtual int FindMin(int (*cmp)(T,T)) = 0;

  virtual void Swap(int i, int j) = 0;

  virtual std::vector<T> ToVector() = 0;

  virtual ~Sequence<T>() = default;
};