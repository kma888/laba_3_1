#pragma once
#include <iostream>
using namespace std;

template<class T>
struct Item {
  T value;
  Item<T> *prev = nullptr;
  Item<T> *next = nullptr;

  Item(T value1, Item<T> *prev1, Item<T> *next1) : value(value1), prev(prev1), next(next1) {}

  ~Item() = default;
};

template<class T>
class LinkedList {
private:
  Item<T> *first = nullptr;
  Item<T> *last = nullptr;
  int size;

public:
  LinkedList() : first(nullptr), last(nullptr), size(0) {}

  LinkedList(T *list, size_t size_) : LinkedList() {
    for (int i = 0; i < size_; i++) {
      Append(list[i]);
    }
  }

  LinkedList(const LinkedList<T> &list) {
    size = list.GetSize();
    for (auto i = list.first; i != nullptr; i = i->next) {
      auto *item = new Item<T>(i->value, last, nullptr);
      if (first == nullptr)
        first = item;
      if (last != nullptr)
        last->next = item;
      last = item;
    }
  }

  /////////////////////////////////////////////////////////////////////////

  int GetSize() const {return size;}

  void Set(T item, int index) { GetNode(index)->value = item; }

  void Delete(int index) {
      if (GetSize()==1) {
          first = NULL;
          last = NULL;
          size = 0;
          return;
      }
      if (index==0) {
          first = first->next;
          first->prev = NULL;
      }
      else if (index==GetSize()-1) {
          last = last->prev;
          last->next = NULL;
      }
      else {
          Item<T> *tmp1 = GetNode(index - 1);
          Item<T> *tmp2 = GetNode(index + 1);
          tmp1->next = tmp2;
          tmp2->prev = tmp1;
      }
      size--;
  }

  void Resize(size_t size_) {
    int old_size = GetSize();
    if (size_ <= old_size) {
      while (old_size != size_) {
        Item<T> *item = last;
        last = last->prev;
        delete item;
        old_size--;
      }
      last->next = nullptr;
    } else {
      while (old_size != size_) {
        auto *item = new Item<T>(0, last, nullptr);
        last->next = item;
        last = item;
        old_size++;
      }
    }
    size = size_;
  }

  void Append(T item) {
    auto elem = new Item<T>(item, last, nullptr);
    if (first == nullptr)
      first = elem;
    if (last != nullptr)
      last->next = elem;
    last = elem;
    size++;
  }

  void Prepend(T& item){
    auto elem = new Item<T>(item, nullptr, first);
    if (first != nullptr)
      first->prev = elem;
    first = elem;
    if (last == nullptr)
      last = elem;
    size++;
  }

  void Print() {
      int counter = 0;
    for (auto i = first; i != nullptr; i = i->next){
      cout << i->value << "\t";
      counter++;
    }
    cout << "\n";
  }

  void InsertAt(T& item, int index) {
    assert(index >= 0 && index < GetSize() + 1);
    if (GetSize() == 0) {
      first = new Item<T>(item, nullptr, nullptr);
      last = new Item<T>(item, nullptr, nullptr);
    } else {
      if (index == 0) {
        auto *elem = new Item<T>(item, nullptr, first);
        first->prev = elem;
        first = elem;
        size++;
      } else {
          size++;
        Item<T> *i = first;
        for (int j = 0; j < index - 1; j++)
          i = i->next;
        auto *elem = new Item<T>(item, i, i->next);
        i->next = elem;
        if (elem->next == nullptr) {
          last = elem;
        } else {
          elem->next->prev = elem;
        }
      }
    }
  }

  /*
  void where(bool(*func)(T&)){
    auto tmp = first;
    while (tmp){
      if (func(tmp->value->Get())){
        tmp = tmp->next;
      }
      else{
        for (auto i=tmp; i->next!=NULL; i = i->next)
          i->value = i->next->value;
        delete last;
        last = last->prev;
        last->next = NULL;
        size--;
      }
    }
  }
   */

  Item<T>* GetNode(size_t index) const {
    int j = 0;
    for (Item<T> *i = first; i != nullptr; i = i->next) {
      if (j == index)
        return i;
      j++;
    }
    return nullptr;
  }

  T& operator[](size_t index) const {
      int j = 0;
      for (Item<T> *i = first; i != nullptr; i = i->next) {
          if (j == index)
              return i->value;
          j++;
      }
  }

  T& GetFirst() const { return first->value; }

  T& GetLast() const { return last->value; }

  /*
  Data<T>* reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) {
    auto new_data = first->value->copy_new();
    for (auto i=first; i!=NULL; i=i->next){
      new_data = func(*new_data, *i->value);
    }
    return new_data;
  }
   */

  void Swap(int i, int j){
      Item<T>* k = this->GetNode(i);
      T i_value = k->value;
      Item<T>* i_node = k;
      while ( i!=j ) {
          if (i < j) {
              k = k->next;
              i++;
          }
          else {
              k = k->prev;
              i--;
          }
      }
      T j_value = k->value;
      k->value = i_value;
      i_node->value = j_value;
  }

  int FindMin(int (*cmp)(T,T)) {
      T MinVal = first->value;
      int counter_min = 0;
      Item<T>* tmp = first;
      for (int i=0; i<GetSize(); i++) {
          if ( cmp(MinVal, tmp->value) == 1 ) {
              MinVal = tmp->value;
              counter_min = i;
          }
          tmp = tmp->next;
      }
      return counter_min;
  }

  ~LinkedList() {
    while (first != nullptr) {
      Item<T> *item = first;
      first = first->next;
      delete item;
    }
    first = nullptr;
    last = nullptr;
  }
};