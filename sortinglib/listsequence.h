#pragma once

#include "linkedlist.h"
#include "sequence.h"

template<class T>
class ListSequence : public Sequence<T> {
private:
  LinkedList<T> items;
public:
  ListSequence() : items() {}

  ListSequence(const ListSequence<T> &list) : items(list.items) {}

  explicit ListSequence(int N) : items() {
      int *arr = new int[N];
      for (int i=0; i<N; ++i) {
          arr[i] = rand() % N + 1;
          Append(arr[i]);
      }
  }

  ////////////////////////////////////////////////////////////////////////////

  int FindMin(int (*cmp)(T,T)) override { return items.FindMin(cmp); }

  Sequence<T>* New() override { return new ListSequence<T>(); }

  int GetSize() const override { return items.GetSize(); }

  void Resize(int size) override { items.Resize(size); }

  void Set(T item, int index) override { items.Set(item, index); }

  T& GetFirst() const override { return items.GetFirst(); }

  T& GetLast() const override { return items.GetLast(); }

  T& operator[](int index) override { return items[index]; }

  void Append(T& item) override { items.Append(item); }

  void Prepend(T& item) override { items.Prepend(item); }

  void InsertAt(T& item, int index) override { items.InsertAt(item, index); }

  void Print() override { items.Print(); }

  Sequence<T> *Concat(Sequence<T> &list) override {
    Sequence<T>* concatenated = new ListSequence<T>(*this);
    for (int i = 0; i < list.GetSize(); i++) {
      concatenated->Append(list[i]);
    }
    return concatenated;
  }

  Sequence<T> *GetSubsequence (int startIndex, int endIndex) override {
    auto *sub = new ListSequence<T>;
    for (int i = startIndex; i < endIndex; i++)
      sub->Append(items[i]);
    return sub;
  }

  Sequence<T>* Clone() override {
    auto new_items = new ListSequence<T>();
    for (int i = 0; i < GetSize(); i++) {
        T* a = new T;
        *a = items[i];
        new_items->Append(*a);
    }
    return new_items;
  }

  void Delete(int index) override { items.Delete(index); }

  /*
  Sequence<T>* CopyNew(int size) override {
    auto *new_items = new ListSequence<T>();
    for (int i = 0; i < size; i++)
      new_items->Append(GetFirst()->copy_new());
    return new_items;
  };
  */

  /*
  void Map(void(*func)(T&)) override{
      for (int i=0; i < GetSize(); i++)
        items[i]->map(func);
  };
   */

  /*
  void Where(bool(*func)(T&)) override{
      items.where(func);
  };
   */

  /*
  Data<T>* Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) override{
    return items.reduce(func);
  }
  */

  virtual std::vector<T> ToVector() {
      std::vector<T> *vec = new std::vector<T>;
      for (int i=0; i<GetSize(); ++i){
          vec->push_back((*this)[i]);
      }
      return *vec;
  };

  void Swap(int i, int j) override { items.Swap(i, j); };

  virtual ~ListSequence<T>() = default;
};
