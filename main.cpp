#include "sortinglib/listsequence.h"
#include "sortinglib/arraysequence.h"
#include "sortinglib/iSorter.h"
#include <ctime>
#include <string>
using namespace std;

int choose_sort_type() {
    int sort_type;
    cout << "Выберите тип сортировки" << "\n";
    cout << "1 – Сортировка с помощью простого выбора" << "\n";
    cout << "2 – Сортировка бинарными встаками" << "\n";
    cout << "3 – Квадратичная сортировка" << "\n";
    cout << "4 – Быстрая сортировка" << "\n";
    cout << "5 – Пирамидальная сортировка" << "\n";
    cin >> sort_type;
    return sort_type;
}

template<class T>
iSorter<T> *choose_sort(int sort_type){
switch (sort_type) {
        case 1:
        {return new SimpleSelectionSort<T>();}
        case 2:
        {return new BinarySort<T>();}
        case 3:
        {return new SquareSort<T>();}
        case 4:
        {return new QuickSort<T>();}
        case 5:
        {return new HeapSort<T>();}
    }
}

template<class T>
void interface_helper(Sequence<T>* as, Sequence<T>* ls, int sort_type){

    as->Print();
    ls->Print();

    cout << "\n";
    clock_t time_req = clock();

    choose_sort<T>(sort_type)->Sort(as, iCompare<T>::Sort)->Print();
    cout << "Reversed:\n";
    choose_sort<T>(sort_type)->Sort(as, iCompare<T>::SortReverse)->Print();
    cout << "For DynamicArray it took " << (float) time_req / CLOCKS_PER_SEC << " seconds" << endl;

    time_req = clock();
    choose_sort<T>(sort_type)->Sort(ls, iCompare<T>::Sort)->Print();
    cout << "Reversed:\n";
    choose_sort<T>(sort_type)->Sort(ls, iCompare<T>::SortReverse)->Print();
    cout << "For LinkedList it took " << (float) time_req / CLOCKS_PER_SEC << " seconds" << endl;
}


void interface() {
    cout << "Программа для сортировки чисел" << "\n" << "Введите 1 для того, чтобы проверить программу на случайных"
                                                        " числах или 2, чтобы ввести свои данные, 3 чтобы выйти" << "\n";
    int variant;
    cin >> variant;

    switch (variant) {
        case 1: {
            int len;
            cout << "Введите длину посл-ти" << "\n";
            cin >> len;

            ArraySequence<int> *as = new ArraySequence<int>;
            ListSequence<int> *ls = new ListSequence<int>;

            int *arr = new int[len];
            for (int _ = 0; _ < len; ++_) {
                arr[_] = rand() % len + 1;
                as->Append(arr[_]);
                ls->Append(arr[_]);
            }

            interface_helper(as, ls, choose_sort_type());
            interface();
        }

        case 2: {
            cout << "С каким типом данных будете работать?" << "\n" << "1. int 2. double 3. string" << "\n";
            int var;
            cin >> var;

            int len;
            cout << "Введите длину посл-ти" << "\n";
            cin >> len;

            switch (var) {
                case 1: {
                    ArraySequence<int> *as = new ArraySequence<int>;
                    ListSequence<int> *ls = new ListSequence<int>;

                    cout << "Введите числа: " << "\n";
                    int *arr = new int[len];
                    for (int _ = 0; _ < len; ++_) {
                        cin >> arr[_];
                        as->Append(arr[_]);
                        ls->Append(arr[_]);
                    }

                    interface_helper(as, ls, choose_sort_type());
                }
                case 2: {
                    ArraySequence<double> *as = new ArraySequence<double>;
                    ListSequence<double> *ls = new ListSequence<double>;

                    cout << "Введите числа: " << "\n";
                    double *arr = new double[len];
                    for (int _ = 0; _ < len; ++_) {
                        cin >> arr[_];
                        as->Append(arr[_]);
                        ls->Append(arr[_]);
                    }
                    interface_helper(as, ls, choose_sort_type());
                }
                case 3: {
                    ArraySequence<string> *as = new ArraySequence<string>;
                    ListSequence<string> *ls = new ListSequence<string>;

                    cout << "Введите слова: " << "\n";
                    string *arr = new string[len];
                    for (int _ = 0; _ < len; ++_) {
                        cin >> arr[_];
                        as->Append(arr[_]);
                        ls->Append(arr[_]);
                    }

                    interface_helper(as, ls, choose_sort_type());
                }
            }
            interface();
        }
        case 3: {
            exit(0);
        }
    }
}

int main(){
    interface();
    return 0;
}