# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/mihail/laba_3_1/sortinglib_benchmarks/lib/test/perf_counters_gtest.cc" "/Users/mihail/laba_3_1/cmake-build-release/sortinglib_benchmarks/lib/test/CMakeFiles/perf_counters_gtest.dir/perf_counters_gtest.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_POSIX_REGEX"
  "HAVE_STD_REGEX"
  "HAVE_STEADY_CLOCK"
  "TEST_BENCHMARK_LIBRARY_HAS_NO_ASSERTIONS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../sortinglib"
  "../sortinglib_benchmarks/lib/include"
  "../sortinglib_benchmarks/lib/src/../include"
  "../sortinglib_tests/lib/googlemock/include"
  "../sortinglib_tests/lib/googlemock"
  "../sortinglib_tests/lib/googletest/include"
  "../sortinglib_tests/lib/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/mihail/laba_3_1/cmake-build-release/sortinglib_benchmarks/lib/src/CMakeFiles/benchmark.dir/DependInfo.cmake"
  "/Users/mihail/laba_3_1/cmake-build-release/sortinglib_tests/lib/googlemock/CMakeFiles/gmock_main.dir/DependInfo.cmake"
  "/Users/mihail/laba_3_1/cmake-build-release/sortinglib_tests/lib/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  "/Users/mihail/laba_3_1/cmake-build-release/sortinglib_tests/lib/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
